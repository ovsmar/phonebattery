import gi
gi.require_version('AppIndicator3', '0.1')
from gi.repository import AppIndicator3 as appindicator
import time
import subprocess
import re

def get_battery_value():
    command = "idevicediagnostics ioregentry AppleARMPMUCharger"
    try:
        output = subprocess.check_output(command, shell=True, text=True)
        pattern = r'<key>CurrentCapacity</key>\s*<integer>(\d+)</integer>'
        match = re.search(pattern, output)
        if match:
            battery_value = match.group(1)
            return battery_value
        else:
            return "Pattern not found in the captured output."
    except subprocess.CalledProcessError:
        return "Failed to retrieve battery information."


if __name__ == "__main__":
    battery_value = get_battery_value()
    print("Battery Value:", battery_value)
    indicator = appindicator.Indicator.new(
        "custom-indicator",
        "bat.png",  
        appindicator.IndicatorCategory.SYSTEM_SERVICES
        
    )
    
    indicator.set_status(appindicator.IndicatorStatus.ACTIVE)


